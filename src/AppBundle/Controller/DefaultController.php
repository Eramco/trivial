<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller{
	
	/**
	 * @Route("/", name="homepage")
	 */
	
	public function welcomeScreenAction(Request $request){
		return $this->render('default/panel.html.twig');
	}
	
    /**
     * @Route("/partida", name="gamepage")
     */
	
	public function boardScreen(Request $request){
		return $this->render('default/tablero.html.twig');
	}
}
