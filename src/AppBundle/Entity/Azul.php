<?php

	/* 
	 * To change this license header, choose License Headers in Project Properties.
	 * To change this template file, choose Tools | Templates
	 * and open the template in the editor.
	 */
	
	namespace AppBundle\Entity;
	use Doctrine\ORM\Mapping as ORM;
	
	/**
	 * Class Azul
	 * @package AppBundle\Entity
	 * 
	 * @ORM\Entity
	 * @ORM\Table(name="azul")
	 * @ORM\Entity(repositoryClass="AppBundle\Entity\AzulRepository")
	 */
	
	class Azul{
		
		/**
		 * @ORM\Column(type="integer")
		 * @ORM\Id
		 * @ORM\GeneratedValue(strategy="AUTO")
		 */
		
		protected $id;
		
		/**
		 * @ORM\Column(type="string")
		 */
		
		protected $question;
		
		/**
		 * @ORM\Column(type="string")
		 */
		
		protected $answer;
	
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set question
     *
     * @param string $question
     *
     * @return Azul
     */
    public function setQuestion($question)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question
     *
     * @return string
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Set answer
     *
     * @param string $answer
     *
     * @return Azul
     */
    public function setAnswer($answer)
    {
        $this->answer = $answer;

        return $this;
    }

    /**
     * Get answer
     *
     * @return string
     */
    public function getAnswer()
    {
        return $this->answer;
    }
}
