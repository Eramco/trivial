<?php

	/* 
	 * To change this license header, choose License Headers in Project Properties.
	 * To change this template file, choose Tools | Templates
	 * and open the template in the editor.
	 */
	
	namespace AppBundle\Entity;
	use FOS\UserBundle\Model\User as BaseUser;
	use Doctrine\ORM\Mapping as ORM;
	
	/**
	 * Class User
	 * @package AppBundle\Entity
	 * 
	 * @ORM\Entity
	 * @ORM\Table(name="USERS")
	 * @ORM\Entity(repositoryClass="AppBundle\Entity\UserRepository")
	 */
	
	class User extends BaseUser{
		
		/**
		 * @ORM\Column(type="integer")
		 * @ORM\Id
		 * @ORM\GeneratedValue(strategy="AUTO")
		 */
		
		protected $id;
		
		public function __construct(){
			parent::__construct();
		}
}
